# Virtual Care - Clinical Scenario API
A hub for all of the API resources needed for our healthcare administration team: clinical scenarios, RX handling, & COVID resources

**This workspace is part of a set of demo workspaces the Postman Solutions Engineering team put together to show how different industries can utilize workspaces to suit their needs, and does not represent a real company. You can read more about the process of creating this workspace in the companion [blog post](https://blog.postman.com/postman-enterprise-healthcare-insurance-industries-leverage-api-response-data/)**  
  
## Workspace Contents

###  Clinical Resources
**Clinical Scenario Collections**: 
These collections walk through the different workflows for setting up a patient scenario that we might encounter in the clinic. Note that these are forked from the original Commure collections and the base URL is replaced with a mock server URL as this workspace is most commonly used as a sandbox environment. If you would like to use these against our production Commure environment, please contact your manager to obtain credentials.
- There are several associated environments that contain the mock server URL. Note that the name of the environments correspond to the name of the collection's fork label: "Capturing data with Forms-Mock", "Care Team Management-Mock", "Independent Medication-Mock", "Telehealth Visit-Mock"

**Lifepack API**: 
The Lifepack API allows us to integrate with Lifepack's prescription management and delivery system so that we can offer patients the option of getting their prescription delivered to their home rather than making a trip to a pharmacy. As this collection is a fork from the original, and the Lifepack team is actively adding new features, it is a good idea to occasionally "pull new changes" from the collection menu, so that we are always developing from the most recent verison.
- Mock Server
- Environtment: "Lifepack-Mock"
- Github integration in version 1.0.0 under `APIs` tab.

**FHIR API**
The FHIR API spec allows for interoperability between participating healthcare organizations.  
In the left vertical sidebar, under the APIs tab, you will find the official FHIR spec. Currently this is a reference for our front-end developers and clients as our backend application is still in development. 
- Associated OpenAPI file under `APIs` Tab > `Define`.

### COVID-19-Related Collections
**Alerts: COVID-19 Tracking**: This collection allows us to set up daily COVID-19 alerts to be sent to our company Slack channel. We currently track all of the counties where we have an office or pop-up care unit. Anyone may add counties of interest by using the steps outlined in the collection or blog post. 

- Read more about setting up Slack alerts in this [blog post](https://blog.postman.com/personalized-covid-19-alert-system/). 
- If you would like to set up an alert, you may add a monitor to do so. Currenlty, there is a monitor set up in each of the office locations' specific workspaces to track the corresponding county's numbers.
- Associated Environment: `Personalized COVID Tracking (fork symbol) covid-alerts`

**COVID Data Resources**:  
This collection aggregates COVID-19 data from several public sources to help us better understand the evolving situation. This was put together by the research team, but we encourage all departments to utilitze the information here when planning and allocating resources. 
- Associated Environment: `COVID-19 Resouces`
- Mock server: `COVID-19 Resouces` 
- Monitor
- Visualizers: several requests have associated data visualizations to make it a little quicker and easier to understand the data. To access these, run the request, then head to the "visualize" tab in the response pane. (click on the link to be taken to the request.)
    - [Test positivity - positive vs total tests by state:](https://healthcare-demo.postman.co/workspace/Virtual-Care~a12dde60-c321-4797-af83-4f2ab255023d/request/16901628-1d84eb79-eeb4-4b29-8b8e-0d878f845837) 
    <img src="https://user-images.githubusercontent.com/20145532/131130843-f7eac8af-2ad0-46f9-94f9-c49bf5150dc3.png" width="700"/>
    - [Confirmed COVID-19 Cases + Deaths over time in the US:](https://healthcare-demo.postman.co/workspace/Virtual-Care~a12dde60-c321-4797-af83-4f2ab255023d/request/16901628-0deed4e4-9d20-48c1-8ecf-975e977ced8a)
    <img src="https://user-images.githubusercontent.com/20145532/131135204-13d0e470-fa9a-4d72-8fa5-c79d153b8cdd.png" width="700"/>
    - [Confirmed cases from the past 14 days, by state:](https://healthcare-demo.postman.co/workspace/Virtual-Care~a12dde60-c321-4797-af83-4f2ab255023d/request/16901628-f04b1aa3-1b24-4e0f-8ef8-f10d643cee24)
    ![covidMapSmall](https://user-images.githubusercontent.com/20145532/131198400-2a456182-f185-43d6-8a27-401d199392e9.gif)
